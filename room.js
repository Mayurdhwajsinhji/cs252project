function Room(name, id, owner, limit, mynum) {
  this.name = name;
  this.id = id;
  this.owner = owner;
  this.people = [];
  this.count = 1;
  this.mynum = mynum;
  this.peopleLimit = limit;
  this.status = "available";
  this.private = false;
};



Room.prototype.addPerson = function(personID) {
  if (this.status === "available") {
    this.people.push(personID);
    this.count = this.count + 1;
  }
};

Room.prototype.removePerson = function(person) {
  var personIndex = -1;
  for(var i = 0; i < this.people.length; i++){
    if(this.people[i].id === person.id){
      personIndex = i;
      break;
    }
  }
  this.people.remove(personIndex);
  this.count = this.count - 1;
};

Room.prototype.getPerson = function(personID) {
  var person = null;
  for(var i = 0; i < this.people.length; i++) {
    if(this.people[i].id == personID) {
      person = this.people[i];
      break;
    }
  }
  return person;
};

Room.prototype.isAvailable = function() {
  return this.available === "available";
};

Room.prototype.isPrivate = function() {
  return this.private;
};

module.exports = Room;
